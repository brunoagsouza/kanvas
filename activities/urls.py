from django.urls import path
from .views import ActivitiesView

urlpatterns = [
    path('', ActivitiesView.as_view()),
    path('<int:user_id>/', ActivitiesView.as_view()),
]
