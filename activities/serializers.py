from rest_framework import serializers
from django.contrib.auth.models import User


class ActivitiesSerializer(serializers.Serializer):

    id = serializers.IntegerField(read_only=True)
    user_id = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())
    repo = serializers.CharField()
    grade = serializers.IntegerField(allow_null=True, default=None)


class ActivitiesUpdateSerializer(serializers.Serializer):

    id = serializers.IntegerField()
    user_id = serializers.IntegerField()
    repo = serializers.CharField()
    grade = serializers.IntegerField()
