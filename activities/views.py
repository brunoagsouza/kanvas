from django.contrib.auth import authenticate
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.authentication import TokenAuthentication
from rest_framework import status
from django.contrib.auth.models import User

from .serializers import ActivitiesSerializer, ActivitiesUpdateSerializer
from .models import Activities


class ActivitiesView(APIView):

    authentication_classes = [TokenAuthentication]

    def post(self, request):
        if request.user.id == None:
            return Response({"info": "Unauthorized"}, status=status.HTTP_401_UNAUTHORIZED)

        try:

            serializer = ActivitiesSerializer(data=request.data)

            if serializer.is_valid():

                data = serializer.data

                user = User.objects.get(id=data["user_id"])

                activitie = Activities.objects.create(
                    user_id=user, repo=data['repo'], grade=None)

                activitie_ser = ActivitiesSerializer(activitie)

                return Response(activitie_ser.data, status=status.HTTP_201_CREATED)

            else:
                return Response({"details": "bad request"}, status=status.HTTP_400_BAD_REQUEST)
        except:
            return Response({"details": "erro interno no servidor"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def get(self, request, user_id=None):

        if request.user.id == None:
            return Response({"info": "Unauthorized"}, status=status.HTTP_401_UNAUTHORIZED)

        try:
            queryset = Activities.objects.all()

            if request.user.is_superuser or request.user.is_staff:

                if user_id:

                    user = User.objects.get(pk=user_id)

                    activities = Activities.objects.filter(user_id=user)

                    serializers = ActivitiesSerializer(activities, many=True)

                    return Response(serializers.data, status=status.HTTP_200_OK)

                else:
                    print(request.user)

                    return Response(ActivitiesSerializer(queryset, many=True).data, status=status.HTTP_200_OK)

            elif (request.user.is_staff or request.user.is_superuser) == False:

                activities = Activities.objects.filter(user_id=request.user)

                serializer = ActivitiesSerializer(activities, many=True)
                print(serializer.data, "activities")

                return Response(serializer.data, status=status.HTTP_200_OK)

        except:
            return Response({"info": "Internal server error"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def put(self, request):

        if request.user.id == None or (request.user.is_superuser or request.user.is_staff) == False:
            return Response({"info": "Unauthorized"}, status=status.HTTP_401_UNAUTHORIZED)

        try:

            serializer = ActivitiesUpdateSerializer(data=request.data)

            if serializer.is_valid():

                data = serializer.data

                activitie = Activities.objects.get(id=data["id"])

                activitie.grade = data["grade"]

                activitie.save()

                serializer = ActivitiesSerializer(activitie)

                return Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
                return Response({"details": "bad request"}, status=status.HTTP_400_BAD_REQUEST)

        except:
            return Response({"info": "Internal server error"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
