from django.db import models
from django.contrib.auth.models import User


# Create your models here.

class Activities(models.Model):

    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    repo = models.TextField(max_length=255)
    grade = models.IntegerField(null=True, default=None)
