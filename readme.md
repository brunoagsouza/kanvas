POST /api/accounts/

user types:

  "is_superuser": false,
  "is_staff": false -> estudante

  "is_superuser": false,
  "is_staff": true -> facilitador

  "is_superuser": true,
  "is_staff": true -> instrutor

REQUEST

{
  "username": string,
  "password": string,
  "is_superuser": boolean,
  "is_staff": boolean
}

RESPONSE STATUS -> HTTP 201

{
  "id": 1,
  "is_superuser": false,
  "is_staff": false,
  "activity_set": [], // esse campo é opcional
  "username": "student"
}

RESPONSE STATUS -> HTTP 400 *dados invalidos

RESPONSE STATUS -> HTTP 500 *erro interno do servidor

POST /api/login/

REQUEST

{
	"username": string,
	"password": string
}

RESPONSE STATUS -> HTTP 200

{
  "token": string
}

RESPONSE STATUS -> HTTP 401 *credenciais fornecidas são invalidas

RESPONSE STATUS -> HTTP 500 *erro interno do servidor

POST /api/activities/

REQUEST

Header -> Authorization: Token <token-do-estudante>

{
	"repo": string,
	"user_id": int,
}

RESPONSE STATUS -> HTTP 201

{
  "id": int,
  "user_id": int,
  "repo": string,
  "grade": null
}

RESPONSE STATUS -> HTTP 401 *token invalido
RESPONSE STATUS -> HTTP 400 *dados da requisição invalidos
RESPONSE STATUS -> HTTP 500 *erro interno do servidor

PUT /api/activities/

REQUEST

Header -> Authorization: Token <token-do-facilitador ou instrutor>
{
  "id": int,
  "repo": string,
  "user_id": int,
  "grade": int
}

RESPONSE STATUS -> HTTP 201
{
  "id": int,
  "user_id": int,
  "repo": string,
  "grade": int
}
RESPONSE STATUS -> HTTP 401 *token invalido
RESPONSE STATUS -> HTTP 400 *dados da requisição invalidos
RESPONSE STATUS -> HTTP 500 *erro interno do servidor

GET /api/activities/<user_id>

*APENAS FACILITADORES E INSTRUTORES PODEM
FILTRAR PELO ID, O RESTO DOS USUARIOS USAM
O ENDPOINT SEM IDENTIFICAÇÃO DO ID NA URL*

REQUEST

Header -> Authorization: Token <token>

RESPONSE STATUS -> HTTP 200
[
  {
    "id": int,
    "user_id": int,
    "repo": string,
    "grade": null or int
  }
]

RESPONSE STATUS -> HTTP 401 *token invalido
RESPONSE STATUS -> HTTP 500 *erro interno do servidor

POST /api/courses/

REQUEST

Header -> Authorization: Token <token-do-instrutor>
{
	"name": string
}

RESPONSE STATUS -> HTTP 201

{
  "id": int,
  "name": string,
  "user_set": list
}

RESPONSE STATUS -> HTTP 403 *usuario não possuí permissão para realizar essa operação
RESPONSE STATUS -> HTTP 400 *dados da requisição invalidos
RESPONSE STATUS -> HTTP 500 *erro interno do servidor

PUT /api/courses/registrations/

REQUEST

Header -> Authorization: Token <token-do-instrutor>
{
	"course_id": int,
	"user_ids": [int, int]
}

RESPONSE STATUS -> HTTP 200
{
  "id": int,
  "name": string,
  "user_set": [
    {
      "id": int,
      "is_superuser": boolean,
      "is_staff": boolean,
      "username": string
    },
    {
      "id": int,
      "is_superuser": boolean,
      "is_staff": boolean,
      "username": string
    }
  ]
}

RESPONSE STATUS -> HTTP 403 *usuario não possuí permissão para realizar essa operação
RESPONSE STATUS -> HTTP 400 *dados da requisição invalidos
RESPONSE STATUS -> HTTP 500 *erro interno do servidor

GET /api/courses/

RESPONSE STATUS -> HTTP 200
[
  {
  "id": int,
  "name": string,
  "user_set": [
    {
      "id": int,
      "is_superuser": boolean,
      "is_staff": boolean,
      "username": string
    },
    {
      "id": int,
      "is_superuser": boolean,
      "is_staff": boolean,
      "username": string
    }
  ]
},
{
  "id": int,
  "name": string,
  "user_set": []
}
]

RESPONSE STATUS -> HTTP 500 *erro interno do servidor