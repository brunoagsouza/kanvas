from django.db import models
from django.contrib.auth.models import User


class Course(models.Model):

    name = models.TextField(max_length=100)
    user_set = models.ManyToManyField(User)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Course"
        verbose_name_plural = "Courses"
