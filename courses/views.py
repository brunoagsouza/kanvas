from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from courses.models import Course
from .serializers import CourseSerializer, UserSerializer, CourseUpdateSerializer
from django.contrib.auth.models import User


class CoursesView(APIView):

    authentication_classes = [TokenAuthentication]

    def post(self, request):
        try:
            user = request.user

            if user.is_superuser:
                course_data = CourseSerializer(data=request.data)
                if course_data.is_valid():
                    course = Course.objects.create(
                        name=request.data['name'])
                    course_data = CourseSerializer(course)
                    course.save()
                    return Response(course_data.data, status=status.HTTP_201_CREATED)

                else:
                    return Response({"details": "bad request"}, status=status.HTTP_400_BAD_REQUEST)

            else:
                return Response({"details": "You do not have permission to perform this action."}, status=status.HTTP_403_FORBIDDEN)
        except:
            return Response({"details": "Internal server error"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def get(self, request):

        try:

            courses = Course.objects.all()

            course_data = CourseSerializer(courses, many=True)

            return Response(course_data.data, status=status.HTTP_200_OK)
        except:
            return Response({"details": "Internal server error"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class CoursesRegistrationView(APIView):

    authentication_classes = [TokenAuthentication]

    def put(self, request):

        try:

            if request.user.is_superuser:

                data = CourseUpdateSerializer(data=request.data)
                if data.is_valid():
                    course = Course.objects.get(id=data.data['course_id'])
                    course.user_set.clear()
                    for user in data.data['user_ids']:

                        course.user_set.add(User.objects.get(id=user))

                    course_data = CourseSerializer(course)
                    return Response(course_data.data, status=status.HTTP_202_ACCEPTED)

                else:
                    return Response({"details": "bad request"}, status=status.HTTP_400_BAD_REQUEST)

            else:
                return Response({"details": "You do not have permission to perform this action."}, status=status.HTTP_403_FORBIDDEN)

        except:
            return Response({"details": "Internal server error"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
