from rest_framework import serializers
from accounts.serializers import UserSerializer


class CourseSerializer(serializers.Serializer):

    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField(max_length=100)
    user_set = UserSerializer(many=True, read_only=True)


class CourseUpdateSerializer(serializers.Serializer):

    course_id = serializers.IntegerField()
    user_ids = serializers.ListField(
        child=serializers.IntegerField(min_value=0))
