from django.urls import path
from .views import CoursesView, CoursesRegistrationView

urlpatterns = [
    path('', CoursesView.as_view()),
    path('registrations/', CoursesRegistrationView.as_view()),
]
