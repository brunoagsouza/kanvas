from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .serializers import UserSerializer
from rest_framework.authtoken.models import Token


class UserView(APIView):

    def post(self, request):

        try:
            data_user = UserSerializer(data=request.data)

            if data_user.is_valid():

                data_user = data_user.data

                if data_user["is_superuser"] == True:

                    data_user = User.objects.create_superuser(
                        username=data_user['username'], password=request.data['password'])

                elif data_user["is_staff"] == True:

                    data_user = User.objects.create_user(
                        username=data_user['username'], password=request.data['password'])
                    data_user.is_staff = True

                elif data_user["is_staff"] == False and data_user["is_superuser"] == False:

                    data_user = User.objects.create_user(
                        username=data_user['username'], password=request.data['password'])

            else:

                return Response({"info": "bad request"}, status=status.HTTP_400_BAD_REQUEST)

            data_user.save()

            user_data = UserSerializer(data_user)

            return Response(user_data.data, status=status.HTTP_201_CREATED)

        except:
            return Response({"info": "internal server error"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class LoginView(APIView):

    def post(self, request):

        try:
            user = authenticate(
                username=request.data['username'], password=request.data['password'])

            if user is not None:

                token = Token.objects.get_or_create(user=user)[0]
                return Response({"token": token.key})

            else:

                return Response(status=status.HTTP_401_UNAUTHORIZED)

        except:

            return Response({"info": "internal server error"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
