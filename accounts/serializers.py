from rest_framework import serializers


class UserSerializer(serializers.Serializer):

  id = serializers.IntegerField(read_only = True)
  password = serializers.CharField(required = True, max_length=100,write_only = True)
  username = serializers.CharField(max_length = 100)
  is_staff = serializers.BooleanField(required=True)
  is_superuser = serializers.BooleanField(required=True)